import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  constructor(private authentication: AuthService, private router:Router ){ }

  login(){
    this.authentication.login();
  }

  logout(){
    this.authentication.logout();
    alert('Sikeres kijelentkezés');
    this.router.navigate(['home']);
  }

}
